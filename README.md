package com.lab3;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.math.BigInteger;
import java.net.Socket;

public class Client {
	public void Auth(BigInteger g, BigInteger N, BigInteger k) {
		try {
			Socket s = new Socket("127.0.0.1", 1234);
			DataOutputStream dos = new DataOutputStream(s.getOutputStream());
			DataInputStream dis = new DataInputStream(s.getInputStream());
			
			String password = "hacker1337";
			String salt = Common.randomString(10);
			System.out.println("[CLIENT]: SALT = " + salt);
			// registration
			
			int x = Common.Hash((password + salt).hashCode());
			BigInteger v = g.pow(x).mod(N);
			System.out.println("[CLIENT]: X = " + x);
			System.out.println("[CLIENT]: v = " + v);
			
			dos.writeUTF(salt);
			dos.writeInt(v.intValue());
			dos.flush();
			
			// registration complete
			// auth:
			
			int a = Common.random(1, Common.maxExp);
			BigInteger A = g.pow(a).mod(N);
			dos.writeInt(A.intValue());
			dos.flush();
			System.out.println("[CLIENT]: A = " + A);
			
			BigInteger B = BigInteger.valueOf(dis.readInt());
			
			BigInteger u = Common.Scramble(A.add(B));
			BigInteger S = B.subtract(k.multiply(g.pow(x).mod(N))).pow(BigInteger.valueOf(a).add(u.multiply(BigInteger.valueOf(x))).intValue()).mod(N);
			BigInteger M = N.xor(g).add(BigInteger.valueOf(salt.hashCode())).add(A).add(B).add(S).mod(N);
			System.out.println("[CLIENT]: S = " + S + ", M = " + M);
			dos.writeInt(M.intValue());
			dos.flush();
			
			int R = A.add(M).add(S).intValue();
			System.out.println("[CLIENT]: R = " + R);
			int serverR = dis.readInt();
			
			if (R != serverR) {
				throw new Exception("server R != client R");
			}
			
			System.out.println("[CLIENT]: Authentication to server successfull!");
			
			dos.close();
			dis.close();
			s.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

package com.lab3;

import java.math.BigInteger;
import java.util.Random;

public class Common {
	static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	public static Random rand = new Random();
	public static int maxExp = 100;

	public static String randomString( int len ){
	   StringBuilder sb = new StringBuilder( len );
	   for( int i = 0; i < len; i++ ) 
	      sb.append( AB.charAt( rand.nextInt(AB.length()) ) );
	   return sb.toString();
	}
	
	public static int random(int min, int max) {
		return min + rand.nextInt(max - min + 1);
	}
	
	public static int Hash(int c) {
		return Math.abs(c) % maxExp;
	}
	
	public static BigInteger Scramble(BigInteger c) {
		return c.mod(BigInteger.valueOf(maxExp));
	}
}

package com.lab3;

import java.math.BigInteger;

public class Main {
	
	public static void main(String[] args) {
		BigInteger g = BigInteger.valueOf(311);
		BigInteger N = BigInteger.valueOf(473);
		BigInteger k = BigInteger.valueOf(3);
		/* send v and salt to server */
		
		Server serv = new Server();
		serv.Start(g, N, k);
		
		Client c = new Client();
		c.Auth(g, N, k);
		
		serv.Stop();
	}
}

package com.lab3;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.math.BigInteger;
import java.net.ServerSocket;
import java.net.Socket;

public class Server implements Runnable {
	BigInteger g, N, k;
	boolean running = false;
	Thread thread;
	
	public void Start(BigInteger g, BigInteger N, BigInteger k) {
		running = true;
		thread = new Thread(this);
		thread.start();
		
		this.g = g;
		this.N = N;
		this.k = k;
	}
	
	public void run() {
		try {
			ServerSocket ss = new ServerSocket(1234);
			while (running) {
				Socket client = ss.accept();
				DataOutputStream dos = new DataOutputStream(client.getOutputStream());
				DataInputStream dis = new DataInputStream(client.getInputStream());
				
				// registration
				
				String salt = dis.readUTF();
				BigInteger v = BigInteger.valueOf(dis.readInt());
				
				// registration complete
				// auth:
				
				int b = Common.random(1, Common.maxExp);
				BigInteger B = k.multiply(v).add(g.pow(b).mod(N)).mod(N);
				dos.writeInt(B.intValue());
				dos.flush();
				System.out.println("[SERVER]: B = " + B);
				
				BigInteger A = BigInteger.valueOf(dis.readInt());
				
				BigInteger u = Common.Scramble(A.add(B));
				BigInteger S = A.multiply(v.pow(u.intValue()).mod(N)).pow(b).mod(N);
				BigInteger M = N.xor(g).add(BigInteger.valueOf(salt.hashCode())).add(A).add(B).add(S).mod(N);
				BigInteger clientM = BigInteger.valueOf(dis.readInt());
				System.out.println("[SERVER]: S = " + S + ", M = " + M);
				
				if (!clientM.equals(M)) {
					throw new Exception("client M != server M");
				}
				
				int R = A.add(M).add(S).intValue();
				System.out.println("[SERVER]: R = " + R);
				dos.writeInt(R);
				dos.flush();
				
				System.out.println("[SERVER]: Client logged in!");
				
				dos.close();
				dis.close();
				client.close();
			}
			ss.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void Stop() {
		running = false;
		thread.interrupt();
	}
}
